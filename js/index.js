// Ensure compatibility across browsers
var requestAnimationFrame = (function(){
    return  window.requestAnimationFrame       ||
            window.webkitRequestAnimationFrame ||
            window.mozRequestAnimationFrame    ||
            window.oRequestAnimationFrame      ||
            window.msRequestAnimationFrame     ||
            function(callback, element){
              window.setTimeout(callback, 1000 / 60);
            };
})();

window.AudioContext = window.AudioContext || window.webkitAudioContext;
navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;

window.onload = function(){

    navigator.getUserMedia({ audio: true }, sound, error);

    var ctx = new AudioContext(),
        audioArray = null,
        streamBuffer = null,
        bufferLength;

    function sound(stream){
        var analyser = ctx.createAnalyser();
        var gainNode = ctx.createGain();

        var node = ctx.createScriptProcessor(2048, 1, 1), 
            values = 0, 
            average = 0;

        analyser.smoothingTimeConstant = 0.2;
        analyser.fftSize = 1024;

        node.onaudioprocess = function () {
            // bitcount is fftsize / 2
            bufferLength = analyser.frequencyBinCount;

            audioArray = new Uint8Array(bufferLength);

            // analyser.getByteFrequencyData(audioArray);

            analyser.getByteTimeDomainData(audioArray);

            var length = audioArray.length;
            
            for (var i = 0; i < length; i++) {
                values += audioArray[i];
            }

            average = values / length;
            // oppdaterGui(average);
            average = values = 0;
        };

        var input = ctx.createMediaStreamSource(stream);
        // var input = ctx.createBufferSource(); // creates a sound source
        // input.buffer = stream;     
        input.connect(analyser);
        analyser.connect(node); // connect media source to analyzer
        node.connect(ctx.destination); 
        // Connect the source to the gain node.
        input.connect(gainNode);

        gainNode.gain.value = 0.5;
        // Connect the gain node to the destination.
        gainNode.connect(ctx.destination);
        input.connect(ctx.destination);
        // input.start(0);   
    }

    function error () {
        console.log(arguments);
    }

    var canvas = document.getElementById('audioCanvas');
    var context = canvas.getContext('2d');

    context.fillStyle = '#ffffff';
    context.fillRect(0, 0, canvas.width, canvas.height);// paint all, at cpu cost!

    function update(){

        context.clearRect(0,0, window.innerWidth, window.innerHeight);

        // for(var i in audioArray){
        //     var v = audioArray[i];
        //     // drawBar(i+5, 750-v, 50, v+50);// paint all, at cpu cost!  
        // }
        drawMidLine();

        drawArcs();
        
        drawCurves();

        drawBars();

        requestAnimationFrame( update );
    }

    $('body').on('click', '#btnPlay', function(e){
        e.preventDefault();
        $(this).html('Pause Stream');
        url = $('#inputUrl').val();

        loadSound(url);

        // var source = ctx.createBufferSource(); // creates a sound source
        // source.buffer = streamBuffer;          
          
        sound(streamBuffer);
    });

    function loadSound(url) {
        var request = new XMLHttpRequest();
        request.open('GET', 'https://cors.io?'+url, true);
        // request.setRequestHeader('Access-Control-Allow-Headers', '*');
        // request.setRequestHeader('Access-Control-Allow-Origin','*');
        request.responseType = 'arraybuffer';
      
        // Decode asynchronously
        request.onload = function() {
          ctx.decodeAudioData(request.response, function(buffer) {
            streamBuffer = buffer;
          }, error);
        }
        request.send();
      }

    function randomColor(){
        return "#"+Math.floor(Math.random()*16777215).toString(16);
    }

    function drawMidLine(){
        context.beginPath();
        context.lineWidth = 2;
        context.strokeStyle = randomColor();
        context.moveTo(0, canvas.height/2);
        context.lineTo(window.innerWidth, canvas.height/2);
        context.stroke();
    }

    function drawCurves(){
        context.lineWidth = 2;
        context.strokeStyle = randomColor();

        context.beginPath();

        var sliceWidth = canvas.width * 1.0 / bufferLength;
        var x = 0, i;

        for(i = 0; i < bufferLength; i++) {

            var v = audioArray[i] / 128.0;
            var y = v * canvas.height/2;

            if(i === 0) {
                context.moveTo(x, y);
            } else {
                context.lineTo(x, y);
            }

            x += sliceWidth;
        }

        context.lineTo(canvas.width, canvas.height/2);
        context.stroke();
    }

    function drawArcs(){
        context.lineWidth = 2;
        context.fillStyle = randomColor();
        context.strokeStyle = randomColor();

        var i, r;
        
        for(i = 0; i < bufferLength; i++){
            r = audioArray[i];
            context.beginPath();
            context.arc(canvas.width/2, canvas.height/5, r , r, 2 * Math.PI);
            context.stroke();
        }
    }

    function drawBars(){

        var barWidth = (canvas.width / bufferLength) * 2.5,
            barHeight,
            x = 0, i;

        for(i = 0; i < bufferLength; i++) {
            barHeight = audioArray[i];

            context.beginPath();
            context.lineWidth = 2;
            context.fillStyle = 'rgb(' + (barHeight+100) + ',50,50)';;
            context.strokeStyle = '#008000';
            context.fillRect(x, canvas.height-barHeight, barWidth, barHeight);// paint all, at cpu cost!
            context.stroke();
            x += barWidth + 1;
        }
    }

    update();
};
